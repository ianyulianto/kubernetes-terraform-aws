output "kube-iam-instance-profile-master-name" {
	value = "${aws_iam_instance_profile.kube-iam-instance-profile-master.name}"
}

output "kube-iam-instance-profile-minion-name" {
	value = "${aws_iam_instance_profile.kube-iam-instance-profile-minion.name}"
}