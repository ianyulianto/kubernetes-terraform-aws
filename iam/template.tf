resource "template_file" "master-iam-role" {
	template = "${file("${path.module}/templates/kubernetes-master-role.json")}"	
}

resource "template_file" "master-iam-policy" {
	template = "${file("${path.module}/templates/kubernetes-master-policy.json")}"	
}

resource "template_file" "minion-iam-role" {
	template = "${file("${path.module}/templates/kubernetes-minion-role.json")}"	
}

resource "template_file" "minion-iam-policy" {
	template = "${file("${path.module}/templates/kubernetes-minion-policy.json")}"	
}