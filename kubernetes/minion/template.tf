resource "template_file" "minion-start-var" {
	template = "${file("${path.module}/scripts/minion-start-var.sh")}"

	vars {
		MASTER_INTERNAL_IP 		= "${var.master-internal-ip}"
	}
}

resource "template_file" "minion-start" {
	template = "${ENV} \r\n \r\n${SCRIPT}"

	vars {
		ENV					= "${template_file.minion-start-var.rendered}"
		SCRIPT 				= "${file("${path.module}/scripts/minion-start.sh")}"
	}
}

resource "template_file" "node-instance-prefix" {
	template = "${PREFIX}-minion"

	vars {
		PREFIX 		= "${var.instance-prefix}"
	}
}

resource "template_file" "asg-name" {
	template = "${PREFIX}-group"

	vars {
		PREFIX 		= "${template_file.node-instance-prefix.rendered}"
	}
}