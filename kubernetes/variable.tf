/*
	Required Variables
*/
variable "cluster-name" {}
variable "aws-region" {}
variable "aws-s3-region" {}
variable "bucket-name" {}
variable "cidr-block-prefix" {}
variable "num-minions" {}
variable "ami-id" {}

variable "master-disk-size" {}
variable "master-disk-type" {}
variable "master-instance-type" {}
variable "master-subnet-id" {}
variable "master-sg-id" {}
variable "iam-instance-profile-master" {}

variable "minion-disk-size" {}
variable "minion-disk-type" {}
variable "minion-instance-type" {}
variable "minion-subnet-ids" {}
variable "minion-sg-id" {}
variable "iam-instance-profile-minion" {}
variable "spot-price" {}


variable "ssh-key-name" {}

/*
	Other Variables
*/
variable "server-package-name" {
	default = "kubernetes-server-linux-amd64.tar.gz"
}

variable "salt-package-name" {
	default = "kubernetes-salt.tar.gz"	
}

variable "instance-prefix" {
	default = "kubernetes"
}