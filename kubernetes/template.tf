# AWS S3 URL
resource "template_file" "aws-s3-url" {
	template 		= "https://s3-${AWS_REGION}.amazonaws.com/${BUCKET}/"

	vars {
		AWS_REGION			= "${var.aws-s3-region}"
		BUCKET				= "${var.bucket-name}"
	}
}

# Server Package Key di S3
resource "template_file" "server-package-key" {
	template 		= "devel/${SERVER_PACKAGE}"

	vars {
		SERVER_PACKAGE		= "${var.server-package-name}"
	}
}
resource "template_file" "server-package-url" {
	template 		= "${S3_URL}${SERVER_PACKAGE}"

	vars {
		S3_URL				= "${template_file.aws-s3-url.rendered}"
		SERVER_PACKAGE		= "${template_file.server-package-key.rendered}"
	}
}


# Salt Package Key di S3
resource "template_file" "salt-package-key" {
	template 		= "devel/${SALT_PACKAGE}"

	vars {
		SALT_PACKAGE		= "${var.salt-package-name}"
	}
}
resource "template_file" "salt-package-url" {
	template 		= "${S3_URL}${SALT_PACKAGE}"

	vars {
		S3_URL				= "${template_file.aws-s3-url.rendered}"
		SALT_PACKAGE		= "${template_file.salt-package-key.rendered}"
	}
}

#S3 Bucket name
resource "template_file" "s3-bucket-name" {
	template = "kubernetes-staging-cluster-${CLUSTER_NAME}"

	vars {
		CLUSTER_NAME			= "${var.cluster-name}"
	}
}

# S3 Policy
resource "template_file" "s3-policy" {
	template = "${file("${path.module}/s3-policy/s3-policy.json")}"

	vars {
		BUCKET 			= "${template_file.s3-bucket-name.rendered}"
	}
}