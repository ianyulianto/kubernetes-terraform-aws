/*
	Required Variables
*/
variable "aws-region" {}
variable "cluster-name" {}
variable "cidr-block-prefix" {}
variable "instance-prefix" {}
variable "service-cluster-ip-range" {}
variable "cluster-ip-range" {}
variable "allocate-node-cidrs" {}
variable "server-package-url" {}
variable "salt-package-url" {}
variable "enable-cluster-monitoring" {}
variable "dns-server-ip" {}
variable "master-ip-range" {}
variable "num-minions" {}

variable "master-disk-size" {}
variable "master-disk-type" {}
variable "master-instance-type" {}
variable "master-subnet-id" {}
variable "master-sg-id" {}
variable "ami-id" {}

variable "iam-instance-profile-master" {}
variable "ssh-key-name" {}


/*
	Other Variables
*/
variable "kube-user" {
	default	= "admin"
}

variable "kube-password" {
	default = "test1190"
}

variable "dns-domain" {
	default = "cluster.ip"
}