output "vpc-id" {
	value	= "${aws_vpc.kube-vpc.id}"
}

output "cidr-block-prefix" {
	value	= "${var.cidr-block-prefix}"
}

output "kube-subnet-a-id" {
	value	= "${aws_subnet.kube-subnet-a.id}"
}

output "kube-subnet-b-id" {
	value	= "${aws_subnet.kube-subnet-b.id}"
}