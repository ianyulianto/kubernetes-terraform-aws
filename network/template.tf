#VPC CIDR Block
resource "template_file" "vpc-cidr-block" {
	template 		= "${PREFIX}.0.0/16"

	vars {
		PREFIX		= "${var.cidr-block-prefix}"
	}
}

#VPC Subnet A CIDR Block
resource "template_file" "subnet-a-cidr-block" {
	template 		= "${PREFIX}.0.0/17"

	vars {
		PREFIX		= "${var.cidr-block-prefix}"
	}
}

# VPC Subnet B CIDR Block
resource "template_file" "subnet-b-cidr-block" {
	template 		= "${PREFIX}.128.0/17"

	vars {
		PREFIX		= "${var.cidr-block-prefix}"
	}
}