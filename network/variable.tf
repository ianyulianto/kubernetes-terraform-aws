/*
	Required Variables
*/
variable "cluster-name" {}
variable "aws-region" {}

/*
	Other Variables
*/
variable "cidr-block-prefix" {
	default			= "172.20"
}
